# Breton translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2016-09-12 06:22+0000\n"
"Last-Translator: Fohanno Thierry <thierry.fohanno@ofis-bzh.org>\n"
"Language-Team: Breton <br@li.org>\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:48+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr ""

#: lomiri-docviewer-app.desktop.in:8
#, fuzzy
#| msgid "Document Viewer"
msgid "Document Viewer"
msgstr "Lenner teulioù"

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr "teulioù;diskouezer;pdf;lenner"

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Darn eus an arguzennoù pourchaset n'int ket reizh."

#: src/app/qml/common/CommandLineProxy.qml:58
#, fuzzy
#| msgid "Open ubuntu-docviewer-app displaying the selected file"
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr "Digeriñ ubuntu-docviewer-app evit diskwel ar restr diuzet"

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Lañsañ gant ur skramm leun"

#: src/app/qml/common/CommandLineProxy.qml:71
#, fuzzy
#| msgid "Open ubuntu-docviewer-app in pick mode. Used for tests only."
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Digeriñ ubuntu-docviewer-app er mod diuzañ. Implijet evit an amprouadoù "
"hepken."

#: src/app/qml/common/CommandLineProxy.qml:77
#, fuzzy
#| msgid ""
#| "Show documents from the given folder, instead of ~/Documents.\n"
#| "The path must exist prior to running ubuntu-docviewer-app"
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""
"Diskouez teulioù an teulias pourchaset e-lec'h ~/Teulioù.\n"
"Ret en d'an hent-se bezañ anezhañ a-raok lañsañ ubuntu-docviewer-app"

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Munudoù"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Restr"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Lec'h"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Ment"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Krouet"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Kemm diwezhañ"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "seurt MIME"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Fazi"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Serriñ"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Meur a deul enporzhiet"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Choaz pehini digeriñ :"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Restr ne c'haller ket ober ganti"
msgstr[1] "Restroù ne c'haller ket ober ganto"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "N'eo ket bet enporzhiet an teul-mañ :"
msgstr[1] "N'eo ket bet enporzhiet an teulioù-mañ :"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Tapout"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Seurt restr dianav"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"N'eo ket kemeret ar restr-mañ e karg.\n"
"Ha fellout a ra deoc'h digeriñ anezhi evel ur restr destenn ?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Nullañ"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Ya"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 Go"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 Mo"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 ko"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 okted"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Diverkañ ar restr"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Diverkañ %1 restr"
msgstr[1] "Diverkañ %1 restr"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Ha sur oc'h e fell deoc'h diverkañ ar restr-mañ da viken ?"
msgstr[1] "Ha sur oc'h e fell deoc'h diverkañ ar restroù-mañ da viken ?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Diverkañ"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Rannañ"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "N'eus bet kavet teul ebet"

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Kevreit ho penveg ouzh forzh peseurt urzhiataer ha riklit restroù en teuliad "
"Teulioù pe enlakait ur benveg lemm-laka gant teulioù ennañ."

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr ""

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Hiziv, da %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Dec'h, da %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr "dd/MM/yyyy hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr "dddd, hh:mm"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Teulioù"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
#, fuzzy
#| msgid "Search"
msgid "Search…"
msgstr "Klask"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
#, fuzzy
#| msgid "Sorting settings"
msgid "Sorting settings…"
msgstr "Arventennoù renkañ"

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr "klask en teulioù..."

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Chom hep diuzañ netra"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Diuzañ pep tra"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "N'eus teul ebet hag a glot gant se"

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr "Gwiriit hag-eñ eo skrivet mat ho koulenn pe esaeit ur goulenn all."

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Hiziv"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Dec'h"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Abretoc'h er sizhun-mañ"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Abretoc'h er miz-mañ"

#: src/app/qml/documentPage/SectionHeader.qml:41
#, fuzzy
#| msgid "Even earlier..."
msgid "Even earlier…"
msgstr "Abretoc'h c'hoazh..."

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Rannañ gant"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Arventennoù renkañ"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Renkañ dre zeiziad (an hini diwezhañ da gentañ)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Renkañ dre anv (A-Z)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Renkañ dre vent (an hini bihanañ da gentañ)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "urzh kontrol"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "O kargañ..."

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "teul testenn LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "Follenn jediñ LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "Kinnigadur LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "Teul tresadenn LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Teul LibreOffice dianav"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
#, fuzzy
#| msgid "Unknown type document"
msgid "Unknown document type"
msgstr "Seut teul dianav"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
#, fuzzy
#| msgid "Go to position"
msgid "Go to position…"
msgstr "Mont d'al lec'h"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "diweredekaat ar mod noz"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Gweredekaat ar mod noz"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Mont d'al lec'h"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Choaz ul lec'h etre 1 % ha 100 %"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "DEOMP !"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "N'eo ket bet kavet danvez binarel LibreOffice"

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "Fazi pa oad o kargañ LibreOffice."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"N'eo ket bet karget an teul.\n"
"Brein pe gwarezet gant ur ger-tremen eo an teul goulennet marteze."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "N'eus netra er follenn-mañ"

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Kaset en-dro gant LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Follenn%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "Keidañ gant al ledander"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Keidañ gant an uhelder"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Emgefreek"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "N'eus ket eus ar restr"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
#, fuzzy
#| msgid "Document Viewer"
msgid "Document is locked"
msgstr "Lenner teulioù"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr ""

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr ""

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, fuzzy, qt-format
#| msgid "Go to page"
msgid "Go to page %1"
msgstr "Mont d'ar bajenn"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
#, fuzzy
#| msgid "Go to position"
msgid "Go to destination"
msgstr "Mont d'al lec'h"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "Taolenn"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Pajenn %1 diwar %2"

#: src/app/qml/pdfView/PdfView.qml:284
msgid "Search"
msgstr "Klask"

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "Mont d'ar bajenn..."

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Kinnigadur"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr ""

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr ""

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Mont d'ar bajenn"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Choaz ur bajenn etre 1 ha %1"

#: src/app/qml/pdfView/ZoomSelector.qml:120
#, fuzzy
#| msgid "Go to page"
msgid "Fit page"
msgstr "Mont d'ar bajenn"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "eilañ %1"

#~ msgid "Search..."
#~ msgstr "Klask..."

#~ msgid "Sorting settings..."
#~ msgstr "Arvetennoù renkañ..."

#~ msgid "Switch to single column list"
#~ msgstr "Ober gant ur roll war ur bann hepken"

#~ msgid "Switch to grid"
#~ msgstr "Ober gant ur gael"

#~ msgid "Back"
#~ msgstr "Distreiñ"

#~ msgid "Go to position..."
#~ msgstr "Mont d'al lec'h..."

#~ msgid "File does not exist"
#~ msgstr "Ar restr n'eus ket anezhi"

#~ msgid "No document found"
#~ msgstr "N'eus bet kavet teul ebet"

#~ msgid "Hide table of contents"
#~ msgstr "Kuzhat an daolenn"
